package com.demo.appname.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import com.demo.appname.entity.Book;
import com.demo.appname.service.api.ApplicationName;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@Transactional
@DatabaseSetup("classpath:/dataset/dataset1.xml")
public class BookTest extends AppTest {

	@Autowired
	private ApplicationName app;

	@Test
	public void test1() {
		Book book = app.books().save(new Book());
		assertTrue(app.books().exists(book.id()));
	}

	@Test
	public void test2() {
		Page<Book> books = app.books().findAllByAuthor("Some Name", null);
		assertTrue(books.getTotalElements() > 0);
	}

}
