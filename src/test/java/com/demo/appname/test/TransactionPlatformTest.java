package com.demo.appname.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaSystemException;

import com.demo.appname.entity.Book;
import com.demo.appname.service.api.BookService;
import com.demo.appname.service.api.PlatformTransactionService;

public class TransactionPlatformTest extends AppTest {

	@Autowired
	private PlatformTransactionService transactions;

	@Autowired
	private BookService books;

	@Test
	public void testSave() {
		long booksCount = books.findAll(null).getTotalElements();
		transactions.executeVoid(status -> {
			books.save(new Book());
		});
		assertTrue(booksCount + 1 == books.findAll(null).getTotalElements());
	}

	@Test(expected = JpaSystemException.class)
	public void testReadonly() {
		transactions.readonlyVoid(status -> {
			books.save(new Book());
		});
		assertTrue("Readonly transaction successfully executed save operation", false);
	}
}
