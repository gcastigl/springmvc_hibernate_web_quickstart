package com.demo.appname.repository.jpa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.appname.entity.Book;

public interface BookRepository extends JpaRepository<Book, Long> {

	Page<Book> findAllByAuthor(String author, Pageable pageable);

}
