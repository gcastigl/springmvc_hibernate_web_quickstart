package com.demo.appname.web.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloWorldController {

	private final Logger logger = LoggerFactory.getLogger(HelloWorldController.class);

	@RequestMapping(value = "/")
	public String index(Map<String, Object> model) {
		logger.debug("index() is executed!");
		return "index";
	}

	@RequestMapping("/test")
	public String helloWorld(Model model) {
		model.addAttribute("message", "Hello World!");
		return "index";
	}

}