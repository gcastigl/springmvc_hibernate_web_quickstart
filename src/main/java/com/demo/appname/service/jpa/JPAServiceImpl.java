package com.demo.appname.service.jpa;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.appname.service.api.JPAService;

public abstract class JPAServiceImpl<T, ID extends Serializable> implements JPAService<T, ID> {

	@Override
	public final Optional<T> findOne(ID id) {
		return Optional.ofNullable(repository().findOne(id));
	}

	@Override
	public final T findOneUnchecked(ID id) {
		return Objects.requireNonNull(repository().findOne(id));
	}

	@Override
	public final T save(T entity) {
		return repository().save(entity);
	}

	@Override
	public boolean exists(ID id) {
		return repository().exists(id);
	}

	@Override
	public final void delete(ID id) {
		repository().delete(id);
	}

	@Override
	public final Page<T> findAll(Pageable pageable) {
		return repository().findAll(pageable);
	}

	protected abstract JpaRepository<T, ID> repository();

}
