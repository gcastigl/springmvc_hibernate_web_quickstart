package com.demo.appname.service.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.appname.service.api.ApplicationName;
import com.demo.appname.service.api.BookService;

@Service
public class ApplicationNameImpl implements ApplicationName {

	@Autowired
	private BookService books;

	@Override
	public BookService books() {
		return books;
	}

}
