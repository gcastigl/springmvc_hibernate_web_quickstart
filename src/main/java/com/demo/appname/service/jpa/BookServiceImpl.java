package com.demo.appname.service.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.demo.appname.entity.Book;
import com.demo.appname.repository.jpa.BookRepository;
import com.demo.appname.service.api.BookService;

@Service
public class BookServiceImpl extends JPAServiceImpl<Book, Long> implements BookService {

	@Autowired
	private BookRepository repository_;

	@Override
	protected JpaRepository<Book, Long> repository() {
		return repository_;
	}

	@Override
	public Page<Book> findAllByAuthor(String author, Pageable pageable) {
		return repository_.findAllByAuthor(author, pageable);
	}

}
