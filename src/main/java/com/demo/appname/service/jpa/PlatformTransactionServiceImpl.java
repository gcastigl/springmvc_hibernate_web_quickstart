package com.demo.appname.service.jpa;

import java.util.function.Consumer;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionTemplate;

import com.demo.appname.service.api.PlatformTransactionService;

@Service
public class PlatformTransactionServiceImpl implements PlatformTransactionService {

	@Autowired
	private PlatformTransactionManager txManager;

	private TransactionTemplate newTemplate() {
		TransactionTemplate template = new TransactionTemplate();
		template.setTransactionManager(txManager);
		template.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		return template;
	}

	@Override
	public <T> T execute(Function<TransactionStatus, T> transaction) {
		return newTemplate().execute(status -> transaction.apply(status));
	}

	@Override
	public void executeVoid(Consumer<TransactionStatus> transaction) {
		newTemplate().execute(status -> {
			transaction.accept(status);
			return null;
		});
	}

	@Override
	public <T> T readonly(Function<TransactionStatus, T> transaction) {
		TransactionTemplate tpt = newTemplate();
		tpt.setReadOnly(true);
		return tpt.execute(status -> transaction.apply(status));
	}

	@Override
	public void readonlyVoid(Consumer<TransactionStatus> transaction) {
		TransactionTemplate tpt = newTemplate();
		tpt.setReadOnly(true);
		tpt.execute(status -> {
			transaction.accept(status);
			return null;
		});
	}

}
