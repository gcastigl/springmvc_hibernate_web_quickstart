package com.demo.appname.service.api;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface JPAService<T, ID extends Serializable> {

	Optional<T> findOne(ID id);

	T findOneUnchecked(ID id);

	T save(T entity);

	boolean exists(ID id);

	void delete(ID id);

	Page<T> findAll(Pageable pageable);

}
