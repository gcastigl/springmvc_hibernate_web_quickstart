package com.demo.appname.service.api;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.demo.appname.entity.Book;

public interface BookService extends JPAService<Book, Long> {

	Page<Book> findAllByAuthor(String author, Pageable pageable);

}
