package com.demo.appname.service.api;

import java.util.function.Consumer;
import java.util.function.Function;

import org.springframework.transaction.TransactionStatus;

public interface PlatformTransactionService {

	<T> T execute(Function<TransactionStatus, T> transaction);

	void executeVoid(Consumer<TransactionStatus> transaction);

	<T> T readonly(Function<TransactionStatus, T> transaction);

	void readonlyVoid(Consumer<TransactionStatus> transaction);

}
