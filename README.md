# Information
This project is a __quick start template__ for __Java Web__ project development.

## Technologies
	1. Maven - Dependency management
	2. JPA (Hibernate) - Persistance layer
	3. Spring - Dependency injection, etc.
	4. JUnit, hamcreat - Testing
	5. Spring MVC - Web view controller
	6. Log4j2 - Logging
	7. Jetty - Servlet engine

# Development

## IDE
	1. Copy and setup parameters in file application.properties.tpt as development.properties
	2. Run Start (Java class) using -DapplicationProperties="file:path/to/development.properties" as parameter

## Console
	1. Copy and setup parameters in file application.properties.tpt as development.properties
	2. mvn jetty:run -DapplicationProperties="file:path/to/development.properties"

# Deployment
To run web application:

	1. mvn clean install
	2. Copy and setup parameters in file application.properties.tpt as deployment.properties
	3. Deploy using -DapplicationProperties="file:path/to/deployment.properties"

If deployment without application specific parameters is needed:

	1. Copy and setup parameters in file application.properties.tpt inside src/main/resources as deployment.properties
	2. Replace line "${applicationProperties}" in file root-context.xml for "classpath:deployment.properties"
	3. mvn clean install

# Testing
To run test execute

	mvn clean test
